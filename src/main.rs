#![cfg_attr(
    all(target_os = "windows", not(debug_assertions)),
    windows_subsystem = "windows"
)]

use std::{net::UdpSocket, thread, time};

use clap::Parser;
use log::{debug, error};
use rosc::{OscPacket, OscType};
use tao::{
    event_loop::{ControlFlow, EventLoopProxy},
    menu::MenuItemAttributes,
    system_tray::SystemTrayBuilder,
    TrayId,
};

type DynResult<T> = Result<T, Box<dyn std::error::Error>>;

fn main() {
    // Initialize logger.
    env_logger::init();

    if let Err(e) = main_inner() {
        error!("An error occurred: {e}");
    }
}

fn main_inner() -> DynResult<()> {
    // Parse command line arguments.
    let opt = Opt::parse();

    // Prepare the theme.
    debug!("Preparing theme");
    let theme_bmp = match opt.theme {
        Theme::Default => {
            tinybmp::RawBmp::from_slice(include_bytes!("../res/themes/default.bmp")).unwrap()
        }
        Theme::Large => {
            tinybmp::RawBmp::from_slice(include_bytes!("../res/themes/large.bmp")).unwrap()
        }
    };
    let theme_bmp_pixel_data = theme_bmp.image_data();
    let mut theme_data = Box::<ThemeData>::default();
    {
        for (digit, char_data) in theme_data.pos_10.iter_mut().enumerate() {
            for (row, char_data_row) in char_data.chunks_mut(16).enumerate() {
                let pixel_data_start = 111 * 160 - row * 160 + digit * 16;
                let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
                char_data_row.copy_from_slice(data);
            }
        }
        for (digit, char_data) in theme_data.pos_1.iter_mut().enumerate() {
            for (row, char_data_row) in char_data.chunks_mut(16).enumerate() {
                let pixel_data_start = 111 * 160 - (row + 16) * 160 + digit * 16;
                let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
                char_data_row.copy_from_slice(data);
            }
        }
        for (digit, char_data) in theme_data.pos_10th.iter_mut().enumerate() {
            for (row, char_data_row) in char_data.chunks_mut(16).enumerate() {
                let pixel_data_start = 111 * 160 - (row + 16 * 2) * 160 + digit * 16;
                let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
                char_data_row.copy_from_slice(data);
            }
        }
        for (digit, char_data) in theme_data.neg_10.iter_mut().enumerate() {
            for (row, char_data_row) in char_data.chunks_mut(16).enumerate() {
                let pixel_data_start = 111 * 160 - (row + 16 * 3) * 160 + digit * 16;
                let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
                char_data_row.copy_from_slice(data);
            }
        }
        for (digit, char_data) in theme_data.neg_1.iter_mut().enumerate() {
            for (row, char_data_row) in char_data.chunks_mut(16).enumerate() {
                let pixel_data_start = 111 * 160 - (row + 16 * 4) * 160 + digit * 16;
                let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
                char_data_row.copy_from_slice(data);
            }
        }
        for (digit, char_data) in theme_data.neg_10th.iter_mut().enumerate() {
            for (row, char_data_row) in char_data.chunks_mut(16).enumerate() {
                let pixel_data_start = 111 * 160 - (row + 16 * 5) * 160 + digit * 16;
                let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
                char_data_row.copy_from_slice(data);
            }
        }
        for (row, char_data_row) in theme_data.unknown.chunks_mut(16).enumerate() {
            let pixel_data_start = 111 * 160 - (row + 16 * 6) * 160;
            let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
            char_data_row.copy_from_slice(data);
        }
        for (row, char_data_row) in theme_data.neg_inf.chunks_mut(16).enumerate() {
            let pixel_data_start = 111 * 160 - (row + 16 * 6) * 160 + 16;
            let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
            char_data_row.copy_from_slice(data);
        }
        for (row, char_data_row) in theme_data.high.chunks_mut(16).enumerate() {
            let pixel_data_start = 111 * 160 - (row + 16 * 6) * 160 + 2 * 16;
            let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
            char_data_row.copy_from_slice(data);
        }
        for (row, char_data_row) in theme_data.low.chunks_mut(16).enumerate() {
            let pixel_data_start = 111 * 160 - (row + 16 * 6) * 160 + 3 * 16;
            let data = &theme_bmp_pixel_data[pixel_data_start..pixel_data_start + 16];
            char_data_row.copy_from_slice(data);
        }
    }

    // Create the default icon that represents that no value has been received yet.
    debug!("Initializing tray icon");
    let initial_icon = create_icon_rgba(None, &theme_data, opt.color).unwrap();
    let initial_tray_icon = tao::system_tray::Icon::from_rgba(initial_icon, 16, 16)?;

    // Create a tao systray app.
    debug!("Initializing tray menu application");
    let main_tray_id = TrayId::new("main-tray");
    let event_loop = tao::event_loop::EventLoop::with_user_event();
    let event_loop_proxy = event_loop.create_proxy();
    let mut tray_menu = tao::menu::ContextMenu::new();
    let quit = tray_menu.add_item(MenuItemAttributes::new("Quit"));
    let quit_id = quit.id();
    let system_tray = SystemTrayBuilder::new(initial_tray_icon, Some(tray_menu))
        .with_id(main_tray_id)
        .with_tooltip("OSC SPL")
        .build(&event_loop)?;
    let mut system_tray = Some(system_tray);

    // Spawn the UDP listener thread.
    debug!("Spawning udp listener");
    let _udp_thread = thread::spawn(move || {
        let fallible = move |event_loop_proxy: EventLoopProxy<_>| -> DynResult<()> {
            let socket = UdpSocket::bind(opt.address)?;
            socket.set_read_timeout(Some(time::Duration::new(2, 0)))?;

            loop {
                let mut buf = [0; 128];

                let Ok((amt, _src)) = socket.recv_from(&mut buf) else {
                    continue;
                };

                if amt >= buf.len() || amt <= 32 {
                    continue;
                }

                let buf = &buf[0..amt];

                if let Some(mainvolume) = parse_mainvolume(buf) {
                    let icon =
                        create_icon_rgba(Some(mainvolume + opt.offset), &theme_data, opt.color)?;
                    let icon = tao::system_tray::Icon::from_rgba(icon, 16, 16)?;
                    event_loop_proxy.send_event(OscSplEvent::SetIcon(icon))?;
                }
            }
        };

        if let Err(e) = fallible(event_loop_proxy.clone()) {
            error!("An error occurred in the udp thread: {e}");
            if let Err(e) = event_loop_proxy.send_event(OscSplEvent::Quit) {
                error!("Can't gracefully stop tray app ({e})");
            }
        }
    });

    // Run the event loop for the systray menu.
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            tao::event::Event::MenuEvent { menu_id, .. } => {
                if menu_id == quit_id {
                    system_tray.take();
                    *control_flow = ControlFlow::Exit;
                }
            }
            tao::event::Event::UserEvent(OscSplEvent::Quit) => {
                system_tray.take();
                *control_flow = ControlFlow::Exit;
            }
            tao::event::Event::UserEvent(OscSplEvent::SetIcon(icon)) => {
                if let Some(system_tray) = &mut system_tray {
                    system_tray.set_icon(icon);
                } else {
                    system_tray.take();
                    *control_flow = ControlFlow::Exit;
                }
            }
            _ => {}
        }
    });
}

#[derive(Debug)]
enum OscSplEvent {
    Quit,
    SetIcon(tao::system_tray::Icon),
}

/// Command line arguments
#[derive(Debug, Parser)]
#[clap(version, author)]
struct Opt {
    /// The color (hex) of the numbers in the system tray
    #[clap(short, long, default_value = "d40000")]
    color: Color,
    /// A static offset added to the fader value (for calibration to match dbSPL at listening position).
    #[clap(short, long, default_value = "0.0")]
    offset: f32,
    /// The appearance of the numbers in the system tray ("default" or "large").
    #[clap(short, long, default_value = "default")]
    theme: Theme,
    /// The socket address and port of the OSC host (RME totalmix).
    #[clap(short, long, default_value = "127.0.0.1:9001")]
    address: std::net::SocketAddr,
}

/// RGBA color
#[derive(Debug, Clone, Copy)]
struct Color([u8; 4]);

impl std::str::FromStr for Color {
    type Err = std::num::ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut i = 0;
        if s.starts_with('#') {
            i += "#".len();
        }
        let mut color = [0, 0, 0, 255];
        let s_lower = s.trim().to_ascii_lowercase();
        for channel in color.iter_mut().take(3) {
            if let Some(substr) = s_lower.get(i..i + 2) {
                *channel = u8::from_str_radix(substr, 16)?;
                i += 2;
            } else {
                break;
            }
        }
        Ok(Self(color))
    }
}

/// The available themes
#[derive(Debug, Clone, Copy, Default)]
enum Theme {
    #[default]
    Default,
    Large,
}

impl std::str::FromStr for Theme {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.to_ascii_lowercase().as_str() {
            "default" => Ok(Self::Default),
            "large" => Ok(Self::Large),
            _ => Err(format!("The theme \"{}\" doesn't exist", s)),
        }
    }
}

/// The pixel data of a theme/font
struct ThemeData {
    pos_10: [[u8; 256]; 10],
    pos_1: [[u8; 256]; 10],
    pos_10th: [[u8; 256]; 10],
    neg_10: [[u8; 256]; 10],
    neg_1: [[u8; 256]; 10],
    neg_10th: [[u8; 256]; 10],
    unknown: [u8; 256],
    neg_inf: [u8; 256],
    high: [u8; 256],
    low: [u8; 256],
}

impl Default for ThemeData {
    fn default() -> Self {
        Self {
            pos_10: [[0u8; 256]; 10],
            pos_1: [[0u8; 256]; 10],
            pos_10th: [[0u8; 256]; 10],
            neg_10: [[0u8; 256]; 10],
            neg_1: [[0u8; 256]; 10],
            neg_10th: [[0u8; 256]; 10],
            unknown: [0u8; 256],
            neg_inf: [0u8; 256],
            high: [0u8; 256],
            low: [0u8; 256],
        }
    }
}

/// Mapping of gray shades to alpha values accordingly to the 8-bit color table
fn theme_pixel_alpha(value: u8) -> u8 {
    0xff - match value {
        7 => 0xc0,
        8 => 0x80,
        15 => 0xff,
        16 => 0x00,
        231 => 0xff,
        232 => 0x08,
        233 => 0x12,
        234 => 0x1c,
        235 => 0x26,
        236 => 0x30,
        237 => 0x3a,
        239 => 0x44,
        240 => 0x4e,
        241 => 0x58,
        242 => 0x62,
        243 => 0x6c,
        244 => 0x76,
        245 => 0x80,
        246 => 0x8a,
        247 => 0x94,
        248 => 0x9e,
        249 => 0xa8,
        250 => 0xb2,
        251 => 0xbc,
        252 => 0xc6,
        253 => 0xd0,
        254 => 0xda,
        // 255 => 0xee,
        v => v,
    }
}

/// Add a layer on top of the given 16x16 pixel icon
fn add_icon_layer(layer_data: &[u8; 256], icon: &mut [u8], color: Color) {
    for (pixel, layer_pixel) in icon.chunks_exact_mut(4).zip(layer_data.iter()) {
        let mut pixel_color = color.0;
        pixel_color[3] = pixel[3].saturating_add(theme_pixel_alpha(*layer_pixel));
        pixel.copy_from_slice(&pixel_color);
    }
}

/// Create a tray icon for the given db SPL value
fn create_icon_rgba(
    value: Option<f32>,
    theme_data: &ThemeData,
    color: Color,
) -> DynResult<Vec<u8>> {
    let mut rgba_data = vec![0u8; 4 * 16 * 16];
    if let Some(value) = value {
        if value == f32::NEG_INFINITY {
            add_icon_layer(&theme_data.neg_inf, &mut rgba_data, color);
        } else {
            let value_int = (value * 10.0).round() as isize;
            if value_int > 999 {
                add_icon_layer(&theme_data.high, &mut rgba_data, color);
            } else if value_int < -999 {
                add_icon_layer(&theme_data.low, &mut rgba_data, color);
            } else if value_int < 0 {
                let value_abs = value_int.abs();
                let n10 = value_abs / 100;
                let n1 = (value_abs - n10 * 100) / 10;
                let n10th = (value_abs - n10 * 100) - n1 * 10;
                add_icon_layer(&theme_data.neg_10th[n10th as usize], &mut rgba_data, color);
                add_icon_layer(&theme_data.neg_1[n1 as usize], &mut rgba_data, color);
                add_icon_layer(&theme_data.neg_10[n10 as usize], &mut rgba_data, color);
            } else {
                let p10 = value_int / 100;
                let p1 = (value_int - p10 * 100) / 10;
                let p10th = (value_int - p10 * 100) - p1 * 10;
                add_icon_layer(&theme_data.pos_10th[p10th as usize], &mut rgba_data, color);
                add_icon_layer(&theme_data.pos_1[p1 as usize], &mut rgba_data, color);
                add_icon_layer(&theme_data.pos_10[p10 as usize], &mut rgba_data, color);
            }
        }
    } else {
        add_icon_layer(&theme_data.unknown, &mut rgba_data, color);
    }

    Ok(rgba_data)
}

/// Try to parse incoming data as volume change
fn parse_mainvolume(buf: &[u8]) -> Option<f32> {
    let packet = match rosc::decoder::decode_udp(buf) {
        Ok((_, OscPacket::Bundle(x))) => x,
        _ => return None,
    };

    let message = match packet.content.get(1) {
        Some(OscPacket::Message(ref x)) => x,
        _ => return None,
    };

    debug!("Received OSC message with addr: {}", message.addr);

    if message.addr != "/1/mastervolumeVal" {
        return None;
    }

    let arg = message.args.first()?;

    let value = match arg {
        OscType::String(ref x) => x,
        _ => return None,
    };

    let value_trimmed = value.split_whitespace().next().unwrap();
    match value_trimmed.parse::<f32>() {
        Ok(value_parsed) => Some(value_parsed),
        Err(_) if value_trimmed == "-oo" => Some(std::f32::NEG_INFINITY),
        _ => None,
    }
}
